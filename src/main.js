import '@babel/polyfill'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'
import 'mutationobserver-shim'
import "vue-select/dist/vue-select.css";
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
require('./plugins')

Vue.config.productionTip = false

new Vue({
  router,
  store,
  // childWindow,
  render: h => h(App)
}).$mount('#app')
