import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        comprobante: {
            detalle:{
                fecha: ''
            }
        }
    },
    mutations: {
        setComprobante(state, comprobante){
            state.comprobante = comprobante
        }
    },
    /* actions: {
        fetchComprobante(context){
            return 
        }
    }, */
    getters: {
        compActualizado: state => state.comprobante
    }
})
