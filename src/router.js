import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import NuevaFactura from './views/Facturas/NuevaFactura.vue'
import Emitidos from './views/Facturas/Emitidos.vue'
import Comprobante from './views/Facturas/Comprobante.vue'
import ListaOrdenes from './views/Ordenes/ListaOrdenes.vue'
import OrdenVer from './views/Ordenes/VerOrden.vue'
import NuevaOrden from './views/Ordenes/NuevaOrden.vue'
import Clientes from './views/Clientes/Clientes.vue'
import ClienteVer from './views/Clientes/Ver.vue'
import ClienteCrear from './views/Clientes/Crear.vue'
import ClienteEditar from './views/Clientes/Editar.vue'
import Tecnicos from './views/Tecnicos/Tecnicos.vue'
import TecnicoVer from './views/Tecnicos/Ver.vue'
import TecnicoCrear from './views/Tecnicos/Crear.vue'
import TecnicoEditar from './views/Tecnicos/Editar.vue'
import Dispositivos from './views/Dispositivos/Dispositivos.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/nueva-factura',
      name: 'nueva-factura',
      component: NuevaFactura
    },
    {
      path: '/emitidos',
      name: 'emitidos',
      component: Emitidos
    },
    {
      path: '/comprobante',
      name: 'comprobante',
      component: Comprobante
    },
    //ORDENES
    {
      path: '/ordenes',
      name: 'ordenes',
      component: ListaOrdenes
    },
    {
      path: '/orden/:id',
      name: 'orden-show',
      component: OrdenVer
    },
    {
      path: '/nueva-orden',
      name: 'nueva-orden',
      component: NuevaOrden
    },
    //CLIENTES
    {
      path: '/clientes',
      name: 'clientes',
      component: Clientes
    },
    {
      path: '/cliente/:id',
      name: 'cliente-show',
      component: ClienteVer
    },
    {
      path: '/cliente/create',
      name: 'cliente-create',
      component: ClienteCrear
    },
    {
      path: '/cliente/:id',
      name: 'cliente-edit',
      component: ClienteEditar
    },
    //Tecnicos
    {
      path: '/tecnicos',
      name: 'tecnicos',
      component: Tecnicos
    },
    {
      path: '/tecnico/:id',
      name: 'tecnico-show',
      component: TecnicoVer
    },
    {
      path: '/tecnico/create',
      name: 'tecnico-create',
      component: TecnicoCrear
    },
    {
      path: '/tecnico/:id',
      name: 'tecnico-edit',
      component: TecnicoEditar
    },
    //Dispositivos
    {
      path: '/dispositivos',
      name: 'dispositivos',
      component: Dispositivos
    },
  ]
})
